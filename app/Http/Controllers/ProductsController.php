<?php

namespace App\Http\Controllers;

use App\Entity\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductsController extends Controller
{

    public function userProducts()
    {
        $products = Product::all()->where('user_id', Auth::id());
        return view('products', compact('products'))->render();
    }
}
